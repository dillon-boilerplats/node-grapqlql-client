import { IValidation, required } from '../../util/validate';

const validate = (values: IValidation) => {
	const errors = required(['title', 'description'], values);

	return errors;
};

export default validate;

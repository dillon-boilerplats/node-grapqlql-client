import { Grid } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import { Keyframes, animated } from 'react-spring';
import delay from 'delay';

import useWindowSize from '../../util/useWindowSize';
import Reveal from './Reveal';

const Slider: any = Keyframes.Spring({
	peek: [{ x: 0, from: { x: -50 }, delay: 500 }, { x: -50, delay: 800 }],
	// single items,
	open: { delay: 0, x: 0 },
	// or async functions with side-effects
	close: async (call: any) => {
		await delay(400);
		await call({ delay: 0, x: -100 });
	}
});

const Content: any = Keyframes.Trail({
	peek: [
		{ x: 0, opacity: 1, from: { x: -50, opacity: 0 }, delay: 600 },
		{ x: -50, opacity: 0, delay: 0 }
	],
	open: { x: 0, opacity: 1, delay: 0 },
	close: { x: -100, opacity: 0, delay: 0 }
});

const SlideIn = () => {
	const windowSize = useWindowSize();
	const [show, setShow] = useState<{ open?: boolean }>({ open: undefined });

	const waitToSlide = async () => {
		await delay(1300);
		setShow({ open: true });
	};

	const fontSize = () => {
		if (windowSize.outerWidth <= 375) {
			return 24;
		} else if (windowSize.outerWidth <= 768) {
			return undefined;
		} else if (windowSize.outerWidth <= 1024) {
			return undefined;
		}
	};

	const items = [
		<Reveal
			text="Dassie Mach 3 data store"
			variant="h3"
			fontSize={fontSize()}
		/>,
		<Reveal
			text="Dassie Mach 3 embodies the dichotomy of technology and agriculture."
			variant="body1"
			fontSize={16}
		/>
	];

	useEffect(() => {
		waitToSlide();
	});

	const state =
		show.open === undefined ? 'peek' : show.open ? 'open' : 'close';

	return (
		<Grid item xs={12}>
			<Slider native state={state}>
				{({ x }: { x: any }) => (
					<animated.div
						className="sidebar"
						style={{
							transform: x.interpolate(
								(x: any) => `translate3d(${x}%,0,0)`
							)
						}}
					>
						<Content
							native
							items={items}
							keys={items.map((_, i) => i)}
							reverse={!show}
							state={state}
						>
							{(item: any, i: any) => ({ x, ...props }: any) => (
								<animated.div
									style={{
										transform: x.interpolate(
											(x: any) => `translate3d(${x}%,0,0)`
										),
										...props
									}}
								>
									{item}
								</animated.div>
							)}
						</Content>
					</animated.div>
				)}
			</Slider>
		</Grid>
	);
};

export default SlideIn;

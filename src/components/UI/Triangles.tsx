import React from 'react';
import { Keyframes, animated } from 'react-spring';

const Container: any = Keyframes.Spring(async (next: any) => {
	while (true) {
		await next({
			from: { radians: 0, color: '#3D55D1' },
			to: { radians: 2 * Math.PI }
		});
	}
});

const Timing = () => {
	const triangles = { items: ['item1', 'item2', 'item3'] };

	const Content = ({ radians, color }: any) =>
		triangles.items.map((__, i) => (
			<animated.svg
				key={i}
				style={{
					width: 50,
					height: 50,
					marginLeft: 20,
					willChange: 'transform',
					transform: radians.interpolate(
						(r: any) =>
							`translate3d(0, ${50 *
								Math.sin(r + (i * 2 * Math.PI) / 5) +
								50}px, 0)`
					)
				}}
				viewBox="0 0 400 400"
			>
				<animated.g fill={color} fillRule="evenodd">
					<svg
						width="100%"
						height="100%"
						viewBox="0 0 88 75"
						fill="none"
						xmlns="http://www.w3.org/2000/svg"
					>
						<path
							fillRule="evenodd"
							clipRule="evenodd"
							d="M87.3013 75L44 0L0.69873 75H87.3013ZM78.641 70L44 10L9.35898 70H78.641Z"
							fill="#3D55D1"
						/>
					</svg>
				</animated.g>
			</animated.svg>
		));

	return (
		<div style={{ height: 150, width: 210 }}>
			<Container
				reset
				native
				keys={triangles.items}
				//impl={TimingAnimation}
				config={{ duration: 2000 /*, easing: Easing.linear*/ }}
			>
				{Content}
			</Container>
		</div>
	);
};

export default Timing;

import React, { useState } from 'react';
import {
	Card,
	CardContent,
	Typography,
	WithStyles,
	withStyles,
	Grid,
	IconButton
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import CheckIcon from '@material-ui/icons/Check';
import { RouteComponentProps } from '@reach/router';
import _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';

import styles from './Home.styles';

import AddTodo from '../../components/addTodo/addTodo';

interface ITodo {
	id: string;
	description: string;
	title: string;
}

interface IProps extends RouteComponentProps, WithStyles<typeof styles> {}

const calc = (x: number, y: number) => [-(y - window.innerHeight / 2) / 20, (x - window.innerWidth / 2) / 20, 1.1]
const trans = (x: number, y: number, s: number) => `perspective(600px) rotateX(${x}deg) rotateY(${y}deg) scale(${s})`

const home = (props: IProps) => {
	const { classes } = props;

	const [todos, setTodos] = useState<ITodo[]>([
		{
			id: '1',
			description: 'Make an app that is good',
			title: 'Make an App'
		}
	]);

	const handleSubmit = (values: any) => {
		const data: ITodo[] = [...todos, { ...values, id: uuidv4() }];

		setTodos(data);
	};

	const handleDelete = (id: string) => {
		const data: ITodo[] = _.filter<ITodo>(todos, o => {
			return o.id !== id;
		});

		setTodos(data);
	};

	return (
		<React.Fragment>
			<AddTodo onSubmit={handleSubmit} />
			{todos.map(todo => {
				return (
					<Card key={todo.id} className={classes.card}>
						<CardContent>
							<Grid container spacing={0}>
								<Grid item xs={10}>
									<Typography
										variant="h5"
										className={classes.title}
										gutterBottom
									>
										{todo.title}
									</Typography>
									<Typography variant="body1" component="p">
										{todo.description}
									</Typography>
								</Grid>
								<Grid item xs={1}>
									<IconButton
										onClick={() => handleDelete(todo.id)}
									>
										<CheckIcon />
									</IconButton>
								</Grid>
								<Grid item xs={1}>
									<IconButton
										onClick={() => handleDelete(todo.id)}
									>
										<DeleteIcon />
									</IconButton>
								</Grid>
							</Grid>
						</CardContent>
					</Card>
				);
			})}
		</React.Fragment>
	);
};

export default withStyles(styles)(home);

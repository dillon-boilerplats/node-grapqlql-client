import { Theme, createStyles } from '@material-ui/core';

const styles = (theme: Theme) =>
	createStyles({
		root: {
			flexGrow: 1
		},
		grid: {
			padding: theme.spacing.unit * 2,
			marginTop: 50,
			maxWidth: 500
		}
	});

export default styles;

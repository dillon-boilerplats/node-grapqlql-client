import React from 'react';
import { Grid, WithStyles, withStyles } from '@material-ui/core';
import { RouteComponentProps } from '@reach/router';

import useWindowSize from '../../util/useWindowSize';
import Triangles from '../../components/UI/Triangles';
import Reveal from '../../components/UI/Reveal';
import SlideIn from '../../components/UI/SlideIn';

import styles from './Loading.styles';

interface IProps extends RouteComponentProps, WithStyles<typeof styles> {}

const Loading = (props: IProps) => {
	const windowSize = useWindowSize();
	const { classes } = props;

	const leftMargin = () => {
		if (windowSize.outerWidth <= 375) {
			return { margin: 0, width: 300 };
		} else if (windowSize.outerWidth <= 706) {
			return { margin: 0, width: 350 };
		} else if (windowSize.outerWidth <= 768) {
			return { margin: 50, width: 450 };
		} else if (windowSize.outerWidth <= 960) {
			return { margin: 100, width: 500 };
		} else if (windowSize.outerWidth <= 1024) {
			return { margin: 150, width: 500 };
		} else {
			return { margin: 300, width: 500 };
		}
	};

	return (
		<div className={classes.root}>
			<Grid
				container
				spacing={24}
				className={classes.grid}
				style={{
					marginLeft: leftMargin().margin,
					maxWidth: leftMargin().width
				}}
			>
				<SlideIn />
				<Grid item xs={12}>
					<Triangles />
				</Grid>
			</Grid>
		</div>
	);
};

export default withStyles(styles)(Loading);
